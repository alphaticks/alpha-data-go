package adata

import (
	"context"
	"encoding/csv"
	"fmt"
	"github.com/stretchr/testify/require"
	registry "gitlab.com/alphaticks/alpha-public-registry-grpc"
	tickstore_types "gitlab.com/alphaticks/tickstore-types"
	"io"
	"math"
	"os"
	"testing"
	"time"
)

var (
	TEST_DURATION = time.Second
)

func TestLocal(t *testing.T) {
	c, err := NewClient("39KpGZmHKj4=", "rxS6ux0OoXybnfY9o2EdoA==", WithStoreAddress("localhost"))
	if err != nil {
		t.Fatal(err)
	}
	it, err := c.NewFloatIterator(DATA_CLIENT_LIVE,
		tickstore_types.WithSelector(`SELECT MidPrice(orderbook) WHERE ID="5485975358912730733"`),
		tickstore_types.WithFrom(0),
		tickstore_types.WithTo(math.MaxUint64),
		tickstore_types.WithStreaming(true),
		tickstore_types.WithTimeout(100*time.Millisecond),
		tickstore_types.WithObjectType("Float64"))
	if err != nil {
		t.Fatal(err)
	}
	end := time.Now().Add(TEST_DURATION)
	for it.Next() && time.Now().Before(end) {
		fmt.Println(it.Time, it.Float)
		require.NotZero(t, it.Float)
		require.NotZero(t, it.Time)
	}
	if it.Error() != nil {
		t.Fatal(it.Error())
	}
	if err := it.Close(); err != nil {
		t.Fatal(err)
	}
}

func TestPortfolio(t *testing.T) {
	c, err := NewClient("39KpGZmHKj4=", "rxS6ux0OoXybnfY9o2EdoA==")
	if err != nil {
		t.Fatal(err)
	}
	now := uint64(time.Now().UnixMilli())

	it, err := c.NewFloatIterator(DATA_CLIENT_WEB3,
		tickstore_types.WithSelector(`SELECT Balance(portfolio, "927") WHERE exchange="hyperliquid" GROUPBY account`),
		tickstore_types.WithFrom(now-100000000),
		tickstore_types.WithTo(now),
		tickstore_types.WithStreaming(false),
		tickstore_types.WithTimeout(1*time.Millisecond))
	if err != nil {
		t.Fatal(err)
	}

	f, _ := os.Create("portfolio.csv")
	cf := csv.NewWriter(f)
	bal := make(map[string]float64)
	for it.Next() {
		fmt.Println(it)
		tags := it.q.Tags()
		bal[tags["account"]] = it.Float
		//v := object.(tickobjects.Float64Object).Float64()
		//bal[tags["account"]] = v
	}
	for k, v := range bal {
		if err := cf.Write([]string{k, fmt.Sprintf("%f", v)}); err != nil {
			t.Fatal(err)
		}
	}
	cf.Flush()
	f.Close()
	if err := it.Close(); err != nil {
		t.Fatalf("error closing query: %v", err)
	}
}

func TestStream(t *testing.T) {
	t.Parallel()
	c, err := NewClient("39KpGZmHKj4=", "rxS6ux0OoXybnfY9o2EdoA==")
	if err != nil {
		t.Fatal(err)
	}
	it, err := c.NewFloatIterator(DATA_CLIENT_LIVE,
		tickstore_types.WithSelector(`SELECT EWMA(Price(trade)) WHERE ID="9281941173829172773"`),
		tickstore_types.WithFrom(uint64(time.Now().UnixMilli())),
		tickstore_types.WithTo(math.MaxUint64),
		tickstore_types.WithStreaming(true),
		tickstore_types.WithTimeout(100*time.Millisecond),
		tickstore_types.WithObjectType("Float64"))
	if err != nil {
		t.Fatal(err)
	}
	end := time.Now().Add(TEST_DURATION)
	for it.Next() && time.Now().Before(end) {
		require.NotZero(t, it.Float)
		require.NotZero(t, it.Time)
	}
	if it.Error() != nil {
		t.Fatal(it.Error())
	}
	if err := it.Close(); err != nil {
		t.Fatal(err)
	}
}

func TestStreamERC721Transfer(t *testing.T) {
	c, err := NewClient("39KpGZmHKj4=", "rxS6ux0OoXybnfY9o2EdoA==")
	if err != nil {
		t.Fatal(err)
	}
	pas, err := c.GetProtocolAssets(context.Background())
	if err != nil {
		t.Fatal(err)
	}
	var bayc *registry.ProtocolAsset
	for _, pa := range pas {
		if pa.ContractAddress != nil && pa.ContractAddress.Value == "0xbce3781ae7ca1a5e050bd9c4c77369867ebc307e" {
			bayc = pa
		}
	}
	it, err := c.StreamERC721Tracker(bayc)
	if err != nil {
		t.Fatal(err)
	}

	it.SetNextDeadline(time.Now())
	for it.Next() {
		fmt.Println(it.Time, it.Tracker.Supply())
	}
	fmt.Println(it.Error())
}

func TestHistoricalERC721Transfer(t *testing.T) {
	//t.Skip()
	c, err := NewClient("39KpGZmHKj4=", "rxS6ux0OoXybnfY9o2EdoA==")
	if err != nil {
		t.Fatal(err)
	}
	pas, err := c.GetProtocolAssets(context.Background())
	if err != nil {
		t.Fatal(err)
	}
	var bayc *registry.ProtocolAsset
	for _, pa := range pas {
		if pa.ContractAddress != nil && pa.ContractAddress.Value == "0xbce3781ae7ca1a5e050bd9c4c77369867ebc307e" {
			bayc = pa
		}
	}
	it, err := c.GetHistoricalERC721Tracker(bayc, TimeSpan{From: 0, To: math.MaxUint64})
	if err != nil {
		t.Fatal(err)
	}

	for it.Next() {
		fmt.Println(it.Tracker.Supply())
	}
}

func TestGetAssets(t *testing.T) {
	t.Parallel()
	c, err := NewClient("39KpGZmHKj4=", "rxS6ux0OoXybnfY9o2EdoA==")
	if err != nil {
		t.Fatal(err)
	}
	_, err = c.GetAssets(context.Background())
	if err != nil {
		t.Fatal(err)
	}
}

func TestGetSecurities(t *testing.T) {
	t.Parallel()
	c, err := NewClient("39KpGZmHKj4=", "rxS6ux0OoXybnfY9o2EdoA==")
	if err != nil {
		t.Fatal(err)
	}
	res, err := c.GetSecurities(context.Background())
	if err != nil {
		t.Fatal(err)
	}
	for _, sec := range res {
		fmt.Println(sec)
	}
}

func TestStreamDepth(t *testing.T) {
	c, err := NewClient("39KpGZmHKj4=", "rxS6ux0OoXybnfY9o2EdoA==")
	if err != nil {
		t.Fatal(err)
	}
	it, err := c.NewFloatIterator(DATA_CLIENT_LIVE,
		tickstore_types.WithSelector(`SELECT AskDepth(orderbook, "1", "true") WHERE exchange="^fbinance$" type="^CRPERP$" base="^DENT$" quote="^USDT$"`),
		tickstore_types.WithStreaming(true),
		tickstore_types.WithFrom(uint64(time.Now().UnixMilli())),
		tickstore_types.WithTo(uint64(time.Now().UnixMilli())+1000))
	if err != nil {
		t.Fatal(err)
	}
	for it.Next() {
		fmt.Println(it.Time, it.Float)
	}
}

func TestStreamMidPrice(t *testing.T) {
	t.Parallel()
	c, err := NewClient("39KpGZmHKj4=", "rxS6ux0OoXybnfY9o2EdoA==")
	if err != nil {
		t.Fatal(err)
	}
	secs, err := c.GetSecurities(context.Background())
	if err != nil {
		t.Fatal(err)
	}
	var sec *registry.Security
	for _, s := range secs {
		if s.Symbol == "BTCUSDT" && s.Exchange == "binance" {
			sec = s
		}
	}
	it, err := c.StreamMidPrice(sec)
	if err != nil {
		t.Fatal(err)
	}
	end := time.Now().Add(TEST_DURATION)
	for it.Next() && time.Now().Before(end) {
		fmt.Println(it.Float)
		require.NotZero(t, it.Float)
		require.NotZero(t, it.Time)
	}
	if it.Error() != nil {
		t.Fatal(it.Error())
	}
	if err := it.Close(); err != nil {
		t.Fatal(err)
	}
}

func TestStreamTrade(t *testing.T) {
	t.Parallel()
	c, err := NewClient("39KpGZmHKj4=", "rxS6ux0OoXybnfY9o2EdoA==")
	if err != nil {
		t.Fatal(err)
	}
	secs, err := c.GetSecurities(context.Background())
	if err != nil {
		t.Fatal(err)
	}
	var sec *registry.Security
	for _, s := range secs {
		if s.Symbol == "BTCUSDT" && s.Exchange == "binance" {
			sec = s
		}
	}
	it, err := c.StreamTrade(sec)
	if err != nil {
		t.Fatal(err)
	}
	end := time.Now().Add(TEST_DURATION)
	for it.Next() && time.Now().Before(end) {
		fmt.Println(it.Price)
		require.NotZero(t, it.Price)
		require.NotZero(t, it.Size)
		require.NotZero(t, it.Time)
	}
	if it.Error() != nil {
		t.Fatal(it.Error())
	}
	if err := it.Close(); err != nil {
		t.Fatal(err)
	}
}

func TestStreamOpenInterest(t *testing.T) {
	t.Parallel()
	c, err := NewClient("39KpGZmHKj4=", "rxS6ux0OoXybnfY9o2EdoA==")
	if err != nil {
		t.Fatal(err)
	}
	secs, err := c.GetSecurities(context.Background())
	if err != nil {
		t.Fatal(err)
	}
	var sec *registry.Security
	for _, s := range secs {
		if s.Symbol == "BTCUSDT" && s.Exchange == "fbinance" {
			sec = s
		}
	}
	it, err := c.StreamOpenInterest(sec)
	if err != nil {
		t.Fatal(err)
	}
	end := time.Now().Add(TEST_DURATION)
	it.SetNextDeadline(end)
	for it.Next() && time.Now().Before(end) {
		require.NotZero(t, it.Float)
		require.NotZero(t, it.Time)
		it.SetNextDeadline(end)
	}
	if it.Error() != nil {
		t.Fatal(it.Error())
	}
	if err := it.Close(); err != nil {
		t.Fatal(err)
	}
}

func TestStreamOHLCV(t *testing.T) {
	t.Parallel()
	c, err := NewClient("39KpGZmHKj4=", "rxS6ux0OoXybnfY9o2EdoA==")
	if err != nil {
		t.Fatal(err)
	}
	secs, err := c.GetSecurities(context.Background())
	if err != nil {
		t.Fatal(err)
	}

	var security *registry.Security
	for _, sec := range secs {
		if sec.Symbol == "ETHUSDT" && sec.Exchange == "binance" {
			security = sec
		}
	}
	if security == nil {
		t.Fatal(err)
	}
	it, err := c.StreamOHLCV(security, 1000)
	if err != nil {
		t.Fatal(err)
	}
	end := time.Now().Add(TEST_DURATION)
	it.SetNextDeadline(end)
	for it.Next() && time.Now().Before(end) {
		require.NotZero(t, it.O)
		require.NotZero(t, it.H)
		require.NotZero(t, it.L)
		require.NotZero(t, it.C)
		require.NotZero(t, it.Price)
		require.NotZero(t, it.Time)
		it.SetNextDeadline(end)
	}
	if it.Error() != nil {
		t.Fatal(it.Error())
	}
	if err := it.Close(); err != nil {
		t.Fatal(err)
	}
}

func TestStreamFunding(t *testing.T) {
	t.Parallel()
	c, err := NewClient("39KpGZmHKj4=", "rxS6ux0OoXybnfY9o2EdoA==")
	if err != nil {
		t.Fatal(err)
	}
	secs, err := c.GetSecurities(context.Background())
	if err != nil {
		t.Fatal(err)
	}

	var security *registry.Security
	for _, sec := range secs {
		if sec.Symbol == "ETHUSDT" && sec.Exchange == "binance" {
			security = sec
		}
	}
	if security == nil {
		t.Fatal(err)
	}
	it, err := c.StreamFundingRate(security)
	if err != nil {
		t.Fatal(err)
	}
	end := time.Now().Add(TEST_DURATION)
	it.SetNextDeadline(end)
	for it.Next() && time.Now().Before(end) {
		fmt.Println(it.Float)
		it.SetNextDeadline(end)
	}
	if it.Error() != nil {
		t.Fatal(it.Error())
	}
	if err := it.Close(); err != nil {
		t.Fatal(err)
	}
}

func TestHistoricalPoolLiquidity(t *testing.T) {
	t.Skip()
	t.Parallel()
	c, err := NewClient("39KpGZmHKj4=", "rxS6ux0OoXybnfY9o2EdoA==")
	if err != nil {
		t.Fatal(err)
	}
	secs, err := c.GetSecurities(context.Background())
	if err != nil {
		t.Fatal(err)
	}

	var security *registry.Security
	for _, sec := range secs {
		if sec.Exchange == "uniswapv3" && sec.Listened {
			security = sec
		}
	}
	fmt.Println(security)
	if security == nil {
		t.Fatal(err)
	}

	it, err := c.NewLiquidityIterator(DATA_CLIENT_WEB3, tickstore_types.WithSelector(fmt.Sprintf(`SELECT PoolLiquidity(unipoolv3, "1000", "0.2", "80") WHERE symbol="%s"`, security.Symbol)))
	if err != nil {
		t.Fatal(err)
	}
	for it.Next() {
		fmt.Println(it.Map)
		fmt.Println(it.Time)
	}
	if it.Error() != io.EOF {
		t.Fatal(it.Error())
	}
	if err := it.Close(); err != io.EOF {
		t.Fatal(err)
	}
}

func TestHistoricalTradePrice(t *testing.T) {
	t.Parallel()
	c, err := NewClient("39KpGZmHKj4=", "rxS6ux0OoXybnfY9o2EdoA==")
	if err != nil {
		t.Fatal(err)
	}
	secs, err := c.GetSecurities(context.Background())
	if err != nil {
		t.Fatal(err)
	}

	var security *registry.Security
	for _, sec := range secs {
		if sec.Symbol == "OCEANUSDT" && sec.Exchange == "fbinance" {
			security = sec
		}
	}
	if security == nil {
		t.Fatal(err)
	}
	from, err := time.Parse(time.RFC3339, "2023-09-01T00:10:05Z")
	if err != nil {
		t.Fatal(err)
	}
	to, err := time.Parse(time.RFC3339, "2023-09-01T12:10:05Z")
	if err != nil {
		t.Fatal(err)
	}

	span := TimeSpan{
		From: uint64(from.UnixNano() / 1000000),
		To:   uint64(to.UnixNano() / 1000000),
	}
	it, err := c.GetHistoricalTradePrice(security, span)
	if err != nil {
		t.Fatal(err)
	}
	for it.Next() {
		fmt.Println(it.Float)
		require.NotZero(t, it.Float)
		require.NotZero(t, it.Time)
	}
	if it.Error() != io.EOF {
		t.Fatal(it.Error())
	}
	if err := it.Close(); err != io.EOF {
		t.Fatal(err)
	}
}

func TestHistoricalFunding(t *testing.T) {
	t.Parallel()
	c, err := NewClient("39KpGZmHKj4=", "rxS6ux0OoXybnfY9o2EdoA==")
	if err != nil {
		t.Fatal(err)
	}
	secs, err := c.GetSecurities(context.Background())
	if err != nil {
		t.Fatal(err)
	}

	var security *registry.Security
	for _, sec := range secs {
		if sec.Symbol == "ETHUSDT" && sec.Exchange == "fbinance" {
			security = sec
		}
	}
	if security == nil {
		t.Fatal(err)
	}
	span := TimeSpan{
		From: uint64(time.Now().UnixNano()/1000000 - 1000000000),
		To:   uint64(time.Now().UnixNano() / 1000000),
	}
	it, err := c.GetHistoricalFundingRate(security, span)
	if err != nil {
		t.Fatal(err)
	}
	for it.Next() {
		require.NotZero(t, it.Float)
		require.NotZero(t, it.Time)
	}
	if it.Error() != io.EOF {
		t.Fatal(it.Error())
	}
	if err := it.Close(); err != io.EOF {
		t.Fatal(err)
	}
}

func TestHistoricalLiquidation(t *testing.T) {
	t.Parallel()
	c, err := NewClient("39KpGZmHKj4=", "rxS6ux0OoXybnfY9o2EdoA==")
	if err != nil {
		t.Fatal(err)
	}
	secs, err := c.GetSecurities(context.Background())
	if err != nil {
		t.Fatal(err)
	}

	var security *registry.Security
	for _, sec := range secs {
		if sec.Symbol == "ETHUSDT" && sec.Exchange == "fbinance" {
			security = sec
		}
	}
	if security == nil {
		t.Fatal(err)
	}
	span := TimeSpan{
		From: uint64(time.Now().UnixNano()/1000000 - 10000000),
		To:   uint64(time.Now().UnixNano() / 1000000),
	}
	it, err := c.GetHistoricalLiquidation(security, span)
	if err != nil {
		t.Fatal(err)
	}
	for it.Next() {
		require.NotZero(t, it.Price)
		require.NotZero(t, it.Size)
		require.NotZero(t, it.Time)
	}
	if it.Error() != io.EOF {
		t.Fatal(it.Error())
	}
	if err := it.Close(); err != io.EOF {
		t.Fatal(err)
	}
}

func TestHistoricalOHLCV(t *testing.T) {
	t.Parallel()
	c, err := NewClient("39KpGZmHKj4=", "rxS6ux0OoXybnfY9o2EdoA==")
	if err != nil {
		t.Fatal(err)
	}
	secs, err := c.GetSecurities(context.Background())
	if err != nil {
		t.Fatal(err)
	}

	var security *registry.Security
	for _, sec := range secs {
		if sec.Symbol == "BTCUSDT" && sec.Exchange == "binance" {
			security = sec
		}
	}
	if security == nil {
		t.Fatal(err)
	}
	span := TimeSpan{
		From: 1667862714185,
		To:   1667866314185,
	}
	it, err := c.GetHistoricalOHLCV(security, FREQUENCY_1M, span)
	if err != nil {
		t.Fatal(err)
	}
	for it.Next() {
		fmt.Println(it.O, it.H, it.L, it.C, it.Time)
		require.NotZero(t, it.O)
		require.NotZero(t, it.H)
		require.NotZero(t, it.L)
		require.NotZero(t, it.C)
		require.NotZero(t, it.Price)
		require.NotZero(t, it.Time)
	}
	if it.Error() != io.EOF {
		t.Fatal(it.Error())
	}
	if err := it.Close(); err != io.EOF {
		t.Fatal(err)
	}
}

func TestGetHistoricalTrade(t *testing.T) {
	t.Parallel()
	c, err := NewClient("39KpGZmHKj4=", "rxS6ux0OoXybnfY9o2EdoA==")
	if err != nil {
		t.Fatal(err)
	}
	secs, err := c.GetSecurities(context.Background())
	if err != nil {
		t.Fatal(err)
	}

	var security *registry.Security
	for _, sec := range secs {
		if sec.Symbol == "ETHUSDT" && sec.Exchange == "binance" {
			security = sec
		}
	}
	if security == nil {
		t.Fatal(err)
	}
	span := TimeSpan{
		From: uint64(time.Now().UnixNano()/1000000 - 100000),
		To:   uint64(time.Now().UnixNano() / 1000000),
	}
	it, err := c.GetHistoricalTrade(security, span)
	if err != nil {
		t.Fatal(err)
	}
	for it.Next() {
		require.NotZero(t, it.Price)
		require.NotZero(t, it.Size)
		require.NotZero(t, it.Time)
	}
	if it.Error() != io.EOF {
		t.Fatal(it.Error())
	}
	if err := it.Close(); err != io.EOF {
		t.Fatal(err)
	}
}

func TestHistoricalOpenInterest(t *testing.T) {
	t.Parallel()
	c, err := NewClient("39KpGZmHKj4=", "rxS6ux0OoXybnfY9o2EdoA==")
	if err != nil {
		t.Fatal(err)
	}
	secs, err := c.GetSecurities(context.Background())
	if err != nil {
		t.Fatal(err)
	}

	var security *registry.Security
	for _, sec := range secs {
		if sec.Symbol == "ETHUSDT" && sec.Exchange == "fbinance" {
			security = sec
		}
	}
	if security == nil {
		t.Fatal(err)
	}
	span := TimeSpan{
		From: uint64(time.Now().UnixNano()/1000000 - 100000),
		To:   uint64(time.Now().UnixNano() / 1000000),
	}
	it, err := c.GetHistoricalOpenInterest(security, span)
	if err != nil {
		t.Fatal(err)
	}
	for it.Next() {
		require.NotZero(t, it.Float)
		require.NotZero(t, it.Time)
	}
	if it.Error() != io.EOF {
		t.Fatal(it.Error())
	}
	if err := it.Close(); err != io.EOF {
		t.Fatal(err)
	}
}

func TestGetHistoricalOHLCV(t *testing.T) {
	t.Parallel()
	c, err := NewClient("39KpGZmHKj4=", "rxS6ux0OoXybnfY9o2EdoA==")
	if err != nil {
		t.Fatal(err)
	}
	secs, err := c.GetSecurities(context.Background())
	if err != nil {
		t.Fatal(err)
	}

	var security *registry.Security
	for _, sec := range secs {
		if sec.Symbol == "BTCUSDT" && sec.Exchange == "binance" {
			security = sec
		}
	}
	if security == nil {
		t.Fatal(err)
	}
	span := TimeSpan{
		From: uint64(time.Now().UnixNano()/1000000 - 100000),
		To:   uint64(time.Now().UnixNano() / 1000000),
	}
	it, err := c.GetHistoricalOHLCV(security, 5000, span)
	if err != nil {
		t.Fatal(err)
	}
	for it.Next() {
		require.NotZero(t, it.O)
		require.NotZero(t, it.H)
		require.NotZero(t, it.L)
		require.NotZero(t, it.C)
		require.NotZero(t, it.Price)
		require.NotZero(t, it.Time)
	}
	if it.Error() != io.EOF {
		t.Fatal(it.Error())
	}
	if err := it.Close(); err != io.EOF {
		t.Fatal(err)
	}
}

func TestGetHistoricalLiquidity(t *testing.T) {
	t.Parallel()
	c, err := NewClient("39KpGZmHKj4=", "rxS6ux0OoXybnfY9o2EdoA==")
	if err != nil {
		t.Fatal(err)
	}
	secs, err := c.GetSecurities(context.Background())
	if err != nil {
		t.Fatal(err)
	}

	var security *registry.Security
	for _, sec := range secs {
		if sec.Symbol == "ETHUSDT" && sec.Exchange == "binance" {
			security = sec
		}
	}
	if security == nil {
		t.Fatal(err)
	}
	span := TimeSpan{
		From: uint64(time.Now().UnixNano()/1000000 - 100000),
		To:   uint64(time.Now().UnixNano() / 1000000),
	}
	it, err := c.GetHistoricalOBLiquidity(security, 60000, 0.2, span)
	if err != nil {
		t.Fatal(err)
	}
	for it.Next() {
	}
	if it.Error() != io.EOF {
		t.Fatal(it.Error())
	}
	if err := it.Close(); err != io.EOF {
		t.Fatal(err)
	}
}

module gitlab.com/alphaticks/alpha-data-go

go 1.21

toolchain go1.21.1

require (
	github.com/stretchr/testify v1.8.2
	gitlab.com/alphaticks/alpha-public-registry-grpc v0.0.0-20220901120624-7d68aa97e411
	gitlab.com/alphaticks/tickstore-go-client v0.0.0-20221125110209-03b7d247711e
	gitlab.com/alphaticks/tickstore-types v0.0.0-20231002105328-eb50d09adf3a
	google.golang.org/grpc v1.51.0
)

require (
	cloud.google.com/go v0.107.0 // indirect
	cloud.google.com/go/compute v1.12.1 // indirect
	cloud.google.com/go/compute/metadata v0.2.1 // indirect
	cloud.google.com/go/iam v0.7.0 // indirect
	cloud.google.com/go/storage v1.28.0 // indirect
	github.com/davecgh/go-spew v1.1.1 // indirect
	github.com/golang/groupcache v0.0.0-20210331224755-41bb18bfe9da // indirect
	github.com/golang/protobuf v1.5.2 // indirect
	github.com/google/go-cmp v0.5.9 // indirect
	github.com/google/uuid v1.3.0 // indirect
	github.com/googleapis/enterprise-certificate-proxy v0.2.0 // indirect
	github.com/googleapis/gax-go/v2 v2.7.0 // indirect
	github.com/melaurent/gotickfile v0.0.0-20220717080531-a4471992e2ff // indirect
	github.com/melaurent/gotickfile/v2 v2.0.0-20230926083754-40190a452b15 // indirect
	github.com/melaurent/kafero v1.2.4-0.20210921082217-5279763aa403 // indirect
	github.com/pmezard/go-difflib v1.0.0 // indirect
	github.com/satori/go.uuid v1.2.0 // indirect
	github.com/wangjia184/sortedset v0.0.0-20220209072355-af6d6d227aa7 // indirect
	gitlab.com/alphaticks/tickstore-grpc v0.0.0-20220513081230-2364b7f3c9bb // indirect
	go.opencensus.io v0.24.0 // indirect
	golang.org/x/net v0.18.0 // indirect
	golang.org/x/oauth2 v0.2.0 // indirect
	golang.org/x/sys v0.14.0 // indirect
	golang.org/x/text v0.14.0 // indirect
	golang.org/x/xerrors v0.0.0-20220907171357-04be3eba64a2 // indirect
	google.golang.org/api v0.103.0 // indirect
	google.golang.org/appengine v1.6.7 // indirect
	google.golang.org/genproto v0.0.0-20221118155620-16455021b5e6 // indirect
	google.golang.org/protobuf v1.28.1 // indirect
	gopkg.in/yaml.v3 v3.0.1 // indirect
)

replace gitlab.com/alphaticks/tickstore-go-client => ../tickstore-go-client

replace gitlab.com/alphaticks/tickstore-types => ../tickstore-types

replace gitlab.com/alphaticks/tickfunctors => ../tickfunctors

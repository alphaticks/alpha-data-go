package adata

import (
	"context"
	"encoding/base64"
	"encoding/binary"
	"fmt"
	"google.golang.org/grpc/credentials/insecure"
	"math"
	"time"

	store "gitlab.com/alphaticks/tickstore-go-client"
	"gitlab.com/alphaticks/tickstore-go-client/config"
	types "gitlab.com/alphaticks/tickstore-types"
	"google.golang.org/grpc/metadata"

	registry "gitlab.com/alphaticks/alpha-public-registry-grpc"
	"google.golang.org/grpc"
)

const (
	DATA_CLIENT_LIVE int64 = -1
	DATA_CLIENT_WEB3 int64 = -2
	DATA_CLIENT_1S   int64 = 1000
	DATA_CLIENT_1M   int64 = DATA_CLIENT_1S * 60
	DATA_CLIENT_1H   int64 = DATA_CLIENT_1M * 60
	DATA_CLIENT_1D   int64 = DATA_CLIENT_1H * 24
)

var ports = map[int64]string{
	DATA_CLIENT_LIVE: "3550",
	DATA_CLIENT_1S:   "3551",
	DATA_CLIENT_1M:   "3552",
	DATA_CLIENT_1H:   "3553",
	DATA_CLIENT_WEB3: "3554",
}

type Client struct {
	licenseID  int64
	licenseKey string
	registry   registry.StaticClient
	raw        *store.RemoteClient
	aggs       map[int64]*store.RemoteClient
	web3       *store.RemoteClient
}

type ClientConfig struct {
	RegistryAddress string
	StoreAddress    string
}

func NewDefaultConfig() *ClientConfig {
	return &ClientConfig{
		RegistryAddress: "registry.alphaticks.io:8021",
		StoreAddress:    "store.alphaticks.io",
	}
}

type ClientOption func(c *ClientConfig)

var WithRegistryAddress = func(address string) ClientOption {
	return func(c *ClientConfig) {
		c.RegistryAddress = address
	}
}

var WithStoreAddress = func(address string) ClientOption {
	return func(c *ClientConfig) {
		c.StoreAddress = address
	}
}

func NewClient(apiKey, apiSecret string, copts ...ClientOption) (*Client, error) {
	cfg := NewDefaultConfig()
	for _, c := range copts {
		c(cfg)
	}
	licenseIDBytes, err := base64.URLEncoding.DecodeString(apiKey)
	if err != nil {
		return nil, fmt.Errorf("invalid api key")
	}
	licenseID := int64(binary.LittleEndian.Uint64(licenseIDBytes))
	licenseIDStr := fmt.Sprintf("%d", licenseID)
	registryAddress := cfg.RegistryAddress
	conn, err := grpc.Dial(registryAddress,
		grpc.WithTransportCredentials(insecure.NewCredentials()),
		grpc.WithDefaultCallOptions(grpc.MaxCallRecvMsgSize(math.MaxInt64)))
	if err != nil {
		return nil, fmt.Errorf("error connecting to public registry gRPC endpoint: %v", err)
	}
	rgstr := registry.NewStaticClient(conn)

	var opts []grpc.DialOption
	opts = append(opts, grpc.WithTransportCredentials(insecure.NewCredentials()))
	opts = append(opts, grpc.WithUnaryInterceptor(func(ctx context.Context, method string, req, reply interface{}, cc *grpc.ClientConn, invoker grpc.UnaryInvoker, opts ...grpc.CallOption) error {
		// Append license-id and license-key
		md := metadata.New(map[string]string{"license-id": licenseIDStr, "license-key": apiSecret})
		ctx = metadata.NewOutgoingContext(ctx, md)
		return invoker(ctx, method, req, reply, cc, opts...)
	}))
	opts = append(opts, grpc.WithStreamInterceptor(func(ctx context.Context, desc *grpc.StreamDesc, cc *grpc.ClientConn, method string, streamer grpc.Streamer, opts ...grpc.CallOption) (stream grpc.ClientStream, err error) {
		// Append license-id and license-key
		md := metadata.New(map[string]string{"license-id": licenseIDStr, "license-key": apiSecret})
		ctx = metadata.NewOutgoingContext(ctx, md)
		return streamer(ctx, desc, cc, method, opts...)
	}))
	opts = append(opts, grpc.WithDefaultCallOptions(grpc.MaxCallRecvMsgSize(math.MaxInt64)))

	stores := make(map[int64]*store.RemoteClient)
	strRaw, err := store.NewRemoteClient(config.StoreClient{Address: cfg.StoreAddress + ":" + ports[DATA_CLIENT_LIVE], Insecure: true}, opts...)
	if err != nil {
		return nil, err
	}
	str1s, err := store.NewRemoteClient(config.StoreClient{Address: cfg.StoreAddress + ":" + ports[DATA_CLIENT_1S], Insecure: true}, opts...)
	if err != nil {
		return nil, err
	}
	str1m, err := store.NewRemoteClient(config.StoreClient{Address: cfg.StoreAddress + ":" + ports[DATA_CLIENT_1M], Insecure: true}, opts...)
	if err != nil {
		return nil, err
	}
	str1h, err := store.NewRemoteClient(config.StoreClient{Address: cfg.StoreAddress + ":" + ports[DATA_CLIENT_1H], Insecure: true}, opts...)
	if err != nil {
		return nil, err
	}
	strweb3, err := store.NewRemoteClient(config.StoreClient{Address: cfg.StoreAddress + ":3554", Insecure: true}, opts...)
	if err != nil {
		return nil, err
	}

	stores[DATA_CLIENT_1S] = str1s
	stores[DATA_CLIENT_1M] = str1m
	stores[DATA_CLIENT_1H] = str1h

	return &Client{
		licenseID:  licenseID,
		licenseKey: apiSecret,
		registry:   rgstr,
		raw:        strRaw,
		aggs:       stores,
		web3:       strweb3,
	}, nil
}

func (c *Client) GetSecurities(ctx context.Context) ([]*registry.Security, error) {
	res, err := c.registry.Securities(ctx, &registry.SecuritiesRequest{})
	if err != nil {
		return nil, err
	}
	return res.Securities, nil
}

func (c *Client) GetAssets(ctx context.Context) ([]*registry.Asset, error) {
	res, err := c.registry.Assets(ctx, &registry.AssetsRequest{})
	if err != nil {
		return nil, err
	}
	return res.Assets, nil
}

func (c *Client) GetProtocols(ctx context.Context) ([]*registry.Protocol, error) {
	res, err := c.registry.Protocols(ctx, &registry.ProtocolsRequest{})
	if err != nil {
		return nil, err
	}
	return res.Protocols, nil
}

func (c *Client) GetProtocolAssets(ctx context.Context) ([]*registry.ProtocolAsset, error) {
	res, err := c.registry.ProtocolAssets(ctx, &registry.ProtocolAssetsRequest{})
	if err != nil {
		return nil, err
	}
	return res.ProtocolAssets, nil
}

func (c *Client) StreamERC721Tracker(asset *registry.ProtocolAsset, configs ...types.Config) (*ERC721TrackerIterator, error) {
	qs := types.NewQuerySettings()
	qs.From = uint64(time.Now().UnixNano() / 1000000)
	qs.To = math.MaxUint64
	qs.Streaming = true
	qs.Selector = fmt.Sprintf(`SELECT erc721transfer WHERE ID="%d"`, asset.ProtocolAssetId)
	for _, c := range configs {
		c(qs)
	}
	q, err := c.getStore(DATA_CLIENT_WEB3).NewQuery(qs)
	if err != nil {
		return nil, fmt.Errorf("error querying store: %v", err)
	}
	return &ERC721TrackerIterator{
		BaseIterator: BaseIterator{q: q},
	}, nil
}

func (c *Client) StreamFundingRate(security *registry.Security, configs ...types.Config) (*FloatIterator, error) {
	qs := types.NewQuerySettings()
	qs.From = uint64(time.Now().UnixNano() / 1000000)
	qs.To = math.MaxUint64
	qs.Streaming = true
	qs.Selector = fmt.Sprintf(`SELECT funding WHERE ID="%d"`, security.SecurityId)
	qs.ObjectType = "Float64"
	for _, c := range configs {
		c(qs)
	}
	q, err := c.getStore(DATA_CLIENT_1H).NewQuery(qs)
	if err != nil {
		return nil, fmt.Errorf("error querying store: %v", err)
	}
	return &FloatIterator{
		BaseIterator: BaseIterator{q: q},
	}, nil
}

func (c *Client) StreamLiquidation(security *registry.Security, configs ...types.Config) (*TradeIterator, error) {
	qs := types.NewQuerySettings()
	qs.From = uint64(time.Now().UnixNano() / 1000000)
	qs.Timeout = 100000
	qs.Streaming = true
	qs.To = math.MaxUint64
	qs.Selector = fmt.Sprintf(`SELECT NewLiquidation(liquidation) WHERE ID="%d"`, security.SecurityId)
	for _, c := range configs {
		c(qs)
	}
	q, err := c.getStore(DATA_CLIENT_1M).NewQuery(qs)
	if err != nil {
		return nil, fmt.Errorf("error querying store: %v", err)
	}
	return &TradeIterator{
		BaseIterator: BaseIterator{q: q},
	}, nil
}

func (c *Client) StreamTrade(security *registry.Security, configs ...types.Config) (*TradeIterator, error) {
	qs := types.NewQuerySettings()
	qs.Timeout = 100000
	qs.Streaming = true
	qs.From = uint64(time.Now().UnixNano() / 1000000)
	qs.To = math.MaxUint64
	qs.Selector = fmt.Sprintf(`SELECT Trade(trade) WHERE ID="%d"`, security.SecurityId)
	for _, c := range configs {
		c(qs)
	}
	q, err := c.raw.NewQuery(qs)
	if err != nil {
		return nil, fmt.Errorf("error querying store: %v", err)
	}
	return &TradeIterator{
		BaseIterator: BaseIterator{q: q},
	}, nil
}

func (c *Client) StreamBestBid(security *registry.Security, configs ...types.Config) (*FloatIterator, error) {
	qs := types.NewQuerySettings()
	qs.Timeout = 100000
	qs.Streaming = true
	qs.From = uint64(time.Now().UnixNano() / 1000000)
	qs.To = math.MaxUint64
	qs.Selector = fmt.Sprintf(`SELECT BestBid(orderbook) WHERE ID="%d"`, security.SecurityId)
	for _, c := range configs {
		c(qs)
	}
	q, err := c.raw.NewQuery(qs)
	if err != nil {
		return nil, fmt.Errorf("error querying store: %v", err)
	}
	return &FloatIterator{
		BaseIterator: BaseIterator{q: q},
	}, nil
}

func (c *Client) StreamMidPrice(security *registry.Security, configs ...types.Config) (*FloatIterator, error) {
	qs := types.NewQuerySettings()
	qs.Timeout = 100000
	qs.Streaming = true
	qs.From = uint64(time.Now().UnixNano() / 1000000)
	qs.To = math.MaxUint64
	qs.Selector = fmt.Sprintf(`SELECT MidPrice(orderbook) WHERE ID="%d"`, security.SecurityId)
	qs.ObjectType = "Float64"
	for _, c := range configs {
		c(qs)
	}
	q, err := c.raw.NewQuery(qs)
	if err != nil {
		return nil, fmt.Errorf("error querying store: %v", err)
	}
	return &FloatIterator{
		BaseIterator: BaseIterator{q: q},
	}, nil
}

func (c *Client) StreamBestAsk(security *registry.Security, configs ...types.Config) (*FloatIterator, error) {
	qs := types.NewQuerySettings()
	qs.Timeout = 100000
	qs.Streaming = true
	qs.From = uint64(time.Now().UnixNano() / 1000000)
	qs.To = math.MaxUint64
	qs.Selector = fmt.Sprintf(`SELECT BestAsk(orderbook) WHERE ID="%d"`, security.SecurityId)
	for _, c := range configs {
		c(qs)
	}
	q, err := c.raw.NewQuery(qs)
	if err != nil {
		return nil, fmt.Errorf("error querying store: %v", err)
	}
	return &FloatIterator{
		BaseIterator: BaseIterator{q: q},
	}, nil
}

func (c *Client) StreamOpenInterest(security *registry.Security, configs ...types.Config) (*FloatIterator, error) {
	qs := types.NewQuerySettings()
	qs.Timeout = 100000
	qs.Streaming = true
	qs.From = uint64(time.Now().UnixNano() / 1000000)
	qs.To = math.MaxUint64
	qs.Selector = fmt.Sprintf(`SELECT openinterest WHERE ID="%d"`, security.SecurityId)
	qs.ObjectType = "Float64"
	for _, c := range configs {
		c(qs)
	}
	q, err := c.getStore(DATA_CLIENT_1M).NewQuery(qs)
	if err != nil {
		return nil, fmt.Errorf("error querying store: %v", err)
	}
	return &FloatIterator{
		BaseIterator: BaseIterator{q: q},
	}, nil
}

func (c *Client) StreamOHLCV(security *registry.Security, frequency Frequency, configs ...types.Config) (*OHLCVIterator, error) {
	qs := types.NewQuerySettings()
	qs.Streaming = true
	qs.From = uint64(time.Now().UnixNano() / 1000000)
	qs.To = math.MaxUint64
	if ok := frequency.check(); !ok {
		return nil, fmt.Errorf("frequency value error")
	}
	qs.Timeout = 100000
	qs.Selector = fmt.Sprintf(`SELECT AggOHLCV(ohlcv, "%d") WHERE ID="%d" GROUPBY base`, frequency, security.SecurityId)
	for _, c := range configs {
		c(qs)
	}
	q, err := c.getStore(int64(frequency)).NewQuery(qs)
	if err != nil {
		return nil, fmt.Errorf("error querying store: %v", err)
	}
	return &OHLCVIterator{
		BaseIterator: BaseIterator{q: q},
	}, nil
}

func (c *Client) StreamOBLiquidity(security *registry.Security, frequency Frequency, depth float64, configs ...types.Config) (*LiquidityIterator, error) {
	if ok := frequency.check(); !ok {
		return nil, fmt.Errorf("frequency value error")
	}
	if frequency < 60_000 {
		return nil, fmt.Errorf("minimum 1m frequency")
	}
	qs := types.NewQuerySettings()
	qs.Streaming = true
	qs.From = uint64(time.Now().UnixNano() / 1000000)
	qs.To = math.MaxUint64
	qs.Timeout = 100000
	qs.Selector = fmt.Sprintf(`SELECT AggOBLiquidity(obliquidity, "%d", "%f", "1000") WHERE ID="%d" GROUPBY base`, frequency, depth, security.SecurityId)
	for _, c := range configs {
		c(qs)
	}
	q, err := c.getStore(int64(frequency)).NewQuery(qs)
	if err != nil {
		return nil, fmt.Errorf("error querying store: %v", err)
	}
	return &LiquidityIterator{
		BaseIterator: BaseIterator{q: q},
	}, nil
}

func (c *Client) GetHistoricalFloat(dataClient int64, timeSpan TimeSpan, configs ...types.Config) (*FloatIterator, error) {
	qs := types.NewQuerySettings()
	qs.Timeout = 100000
	qs.From = timeSpan.From
	qs.To = timeSpan.To
	qs.ObjectType = "Float64"
	for _, c := range configs {
		c(qs)
	}
	q, err := c.getStore(dataClient).NewQuery(qs)
	if err != nil {
		return nil, fmt.Errorf("error querying store: %v", err)
	}
	return &FloatIterator{
		BaseIterator: BaseIterator{q: q},
	}, nil
}

func (c *Client) GetHistoricalERC721Tracker(passet *registry.ProtocolAsset, timeSpan TimeSpan, configs ...types.Config) (*ERC721TrackerIterator, error) {
	qs := types.NewQuerySettings()
	qs.Timeout = 100000
	qs.From = timeSpan.From
	qs.To = timeSpan.To
	qs.Selector = fmt.Sprintf(`SELECT erc721transfer WHERE ID="%d"`, passet.ProtocolAssetId)
	qs.BatchSize = 100000
	for _, c := range configs {
		c(qs)
	}
	q, err := c.web3.NewQuery(qs)
	if err != nil {
		return nil, fmt.Errorf("error querying store: %v", err)
	}
	return &ERC721TrackerIterator{
		BaseIterator: BaseIterator{q: q},
	}, nil
}

func (c *Client) GetHistoricalTradePrice(security *registry.Security, timeSpan TimeSpan, configs ...types.Config) (*FloatIterator, error) {
	qs := types.NewQuerySettings()
	qs.Timeout = 100000
	qs.From = timeSpan.From
	qs.To = timeSpan.To
	qs.Selector = fmt.Sprintf(`SELECT TradePrice(trade) WHERE ID="%d"`, security.SecurityId)
	qs.ObjectType = "Float64"
	qs.BatchSize = 100000
	for _, c := range configs {
		c(qs)
	}
	q, err := c.raw.NewQuery(qs)
	if err != nil {
		return nil, fmt.Errorf("error querying store: %v", err)
	}
	return &FloatIterator{
		BaseIterator: BaseIterator{q: q},
	}, nil
}

func (c *Client) GetHistoricalFundingRate(security *registry.Security, timeSpan TimeSpan, configs ...types.Config) (*FloatIterator, error) {
	qs := types.NewQuerySettings()
	qs.From = timeSpan.From
	qs.To = timeSpan.To
	qs.Selector = fmt.Sprintf(`SELECT funding WHERE ID="%d"`, security.SecurityId)
	qs.ObjectType = "Float64"
	for _, c := range configs {
		c(qs)
	}
	q, err := c.getStore(DATA_CLIENT_1H).NewQuery(qs)
	if err != nil {
		return nil, fmt.Errorf("error querying store: %v", err)
	}
	return &FloatIterator{
		BaseIterator: BaseIterator{q: q},
	}, nil
}

func (c *Client) GetHistoricalLiquidation(security *registry.Security, timeSpan TimeSpan, configs ...types.Config) (*TradeIterator, error) {
	qs := types.NewQuerySettings()
	qs.From = timeSpan.From
	qs.To = timeSpan.To
	qs.Selector = fmt.Sprintf(`SELECT NewLiquidation(liquidation) WHERE ID="%d"`, security.SecurityId)
	for _, c := range configs {
		c(qs)
	}
	q, err := c.getStore(DATA_CLIENT_1M).NewQuery(qs)
	if err != nil {
		return nil, fmt.Errorf("error querying store: %v", err)
	}
	return &TradeIterator{
		BaseIterator: BaseIterator{q: q},
	}, nil
}

func (c *Client) GetHistoricalTrade(security *registry.Security, timeSpan TimeSpan, configs ...types.Config) (*TradeIterator, error) {
	qs := types.NewQuerySettings()
	qs.From = timeSpan.From
	qs.To = timeSpan.To
	qs.Timeout = 100000
	qs.Selector = fmt.Sprintf(`SELECT Trade(trade) WHERE ID="%d"`, security.SecurityId)
	for _, c := range configs {
		c(qs)
	}
	q, err := c.raw.NewQuery(qs)
	if err != nil {
		return nil, fmt.Errorf("error querying store: %v", err)
	}
	return &TradeIterator{
		BaseIterator: BaseIterator{q: q},
	}, nil
}

func (c *Client) GetHistoricalOpenInterest(security *registry.Security, timeSpan TimeSpan, configs ...types.Config) (*FloatIterator, error) {
	qs := types.NewQuerySettings()
	qs.From = timeSpan.From
	qs.To = timeSpan.To
	qs.Timeout = 100000
	qs.Selector = fmt.Sprintf(`SELECT openinterest WHERE ID="%d"`, security.SecurityId)
	qs.ObjectType = "Float64"
	for _, c := range configs {
		c(qs)
	}
	q, err := c.getStore(DATA_CLIENT_1M).NewQuery(qs)
	if err != nil {
		return nil, fmt.Errorf("error querying store: %v", err)
	}
	return &FloatIterator{
		BaseIterator: BaseIterator{q: q},
	}, nil
}

func (c *Client) GetHistoricalOHLCV(security *registry.Security, frequency Frequency, timeSpan TimeSpan, configs ...types.Config) (*OHLCVIterator, error) {
	if ok := frequency.check(); !ok {
		return nil, fmt.Errorf("frequency value error")
	}
	qs := types.NewQuerySettings()
	qs.From = timeSpan.From
	qs.To = timeSpan.To
	qs.Selector = fmt.Sprintf(`SELECT AggOHLCV(ohlcv, "%d") WHERE ID="%d"`, frequency, security.SecurityId)
	qs.Sampler = types.TickSampler{Interval: uint64(frequency)}
	for _, c := range configs {
		c(qs)
	}
	q, err := c.getStore(int64(frequency)).NewQuery(qs)
	if err != nil {
		return nil, fmt.Errorf("error querying store: %v", err)
	}
	return &OHLCVIterator{
		BaseIterator: BaseIterator{q: q},
	}, nil
}

func (c *Client) GetHistoricalOBLiquidity(security *registry.Security, frequency Frequency, depth float64, timeSpan TimeSpan, configs ...types.Config) (*LiquidityIterator, error) {
	if ok := frequency.check(); !ok {
		return nil, fmt.Errorf("frequency value error")
	}
	if frequency < 60_000 {
		return nil, fmt.Errorf("minimum 1m frequency")
	}
	qs := types.NewQuerySettings()
	qs.From = timeSpan.From
	qs.To = timeSpan.To
	qs.Selector = fmt.Sprintf(`SELECT AggOBLiquidity(obliquidity, "%d", "%f", "1000") WHERE ID="%d"`, frequency, depth, security.SecurityId)
	for _, c := range configs {
		c(qs)
	}
	q, err := c.getStore(int64(frequency)).NewQuery(qs)
	if err != nil {
		return nil, fmt.Errorf("error querying store: %v", err)
	}
	return &LiquidityIterator{
		BaseIterator: BaseIterator{q: q},
	}, nil
}

func (c *Client) NewFloatIterator(dataClient int64, configs ...types.Config) (*FloatIterator, error) {
	qs := types.NewQuerySettings()
	qs.ObjectType = "Float64"
	for _, c := range configs {
		c(qs)
	}
	q, err := c.getStore(dataClient).NewQuery(qs)
	if err != nil {
		return nil, fmt.Errorf("error querying store: %v", err)
	}
	return &FloatIterator{
		BaseIterator: BaseIterator{q: q},
	}, nil
}

func (c *Client) NewLiquidityIterator(dataClient int64, configs ...types.Config) (*LiquidityIterator, error) {
	qs := types.NewQuerySettings()
	qs.ObjectType = "OBLiquidity"
	for _, c := range configs {
		c(qs)
	}
	q, err := c.getStore(dataClient).NewQuery(qs)
	if err != nil {
		return nil, fmt.Errorf("error querying store: %v", err)
	}
	return &LiquidityIterator{
		BaseIterator: BaseIterator{q: q},
	}, nil
}

func (c *Client) NewTradeIterator(dataClient int64, configs ...types.Config) (*TradeIterator, error) {
	qs := types.NewQuerySettings()
	qs.ObjectType = "Trade"
	for _, c := range configs {
		c(qs)
	}
	q, err := c.getStore(dataClient).NewQuery(qs)
	if err != nil {
		return nil, fmt.Errorf("error querying store: %v", err)
	}
	return &TradeIterator{
		BaseIterator: BaseIterator{q: q},
	}, nil
}

func (c *Client) NewOHLCVIterator(dataClient int64, configs ...types.Config) (*OHLCVIterator, error) {
	qs := types.NewQuerySettings()
	qs.ObjectType = "OHLCV"
	for _, c := range configs {
		c(qs)
	}
	q, err := c.getStore(dataClient).NewQuery(qs)
	if err != nil {
		return nil, fmt.Errorf("error querying store: %v", err)
	}
	return &OHLCVIterator{
		BaseIterator: BaseIterator{q: q},
	}, nil
}

func (c *Client) NewObjectIterator(dataClient int64, configs ...types.Config) (*ObjectIterator, error) {
	qs := types.NewQuerySettings()
	for _, c := range configs {
		c(qs)
	}

	q, err := c.getStore(dataClient).NewQuery(qs)
	if err != nil {
		return nil, fmt.Errorf("error querying store: %v", err)
	}
	return &ObjectIterator{
		BaseIterator: BaseIterator{q: q},
	}, nil
}

func (c *Client) GetStore(freq int64) *store.RemoteClient {
	return c.getStore(freq)
}

func (c *Client) getStore(freq int64) *store.RemoteClient {
	if freq == DATA_CLIENT_WEB3 {
		return c.web3
	} else if freq == DATA_CLIENT_LIVE {
		return c.raw
	}
	var minScore int64 = math.MaxInt64
	var cfreq int64
	for f := range c.aggs {
		if f <= freq {
			score := freq - f
			if score < minScore {
				minScore = score
				cfreq = f
			}
		}
	}
	return c.aggs[cfreq]
}

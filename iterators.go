package adata

import (
	"math"
	"time"

	types "gitlab.com/alphaticks/tickstore-types"
	"gitlab.com/alphaticks/tickstore-types/tickobjects"
)

type BaseIterator struct {
	q types.TickstoreQuery
}

func (it *BaseIterator) SetNextDeadline(ts time.Time) {
	it.q.SetNextDeadline(ts)
}

func (it *BaseIterator) Error() error {
	return it.q.Err()
}

func (it *BaseIterator) Close() error {
	return it.q.Close()
}

type ObjectIterator struct {
	BaseIterator
	Time time.Time
	Obj  tickobjects.TickObject
}

func (it *ObjectIterator) Next() bool {
	ok := it.q.Next()
	if !ok {
		return false
	}
	tick, obj, _ := it.q.Read()
	it.Time = time.UnixMilli(int64(tick))
	it.Obj = obj
	return true
}

type FloatIterator struct {
	BaseIterator
	Time  time.Time
	Float float64
}

func (it *FloatIterator) Next() bool {
	ok := it.q.Next()
	if !ok {
		return false
	}
	tick, obj, _ := it.q.Read()
	it.Time = time.UnixMilli(int64(tick))
	it.Float = obj.(tickobjects.Float64Object).Float64()
	return true
}

type TradeIterator struct {
	BaseIterator
	Time  time.Time
	Price float64
	Size  float64
	Sell  bool
}

func (it *TradeIterator) Next() bool {
	ok := it.q.Next()
	if !ok {
		return false
	}
	tick, obj, _ := it.q.Read()
	trd := obj.(tickobjects.TradeObject)
	it.Time = time.UnixMilli(int64(tick))
	it.Price = trd.Price()
	it.Size = math.Abs(trd.Size())
	it.Sell = trd.Size() < 0
	return true
}

type OHLCVIterator struct {
	BaseIterator
	Time      time.Time
	O         float32
	H         float32
	L         float32
	C         float32
	BuyCount  uint32
	SellCount uint32
	BuyVol    float32
	SellVol   float32
	Price     float32
}

func (o *OHLCVIterator) Next() bool {
	ok := o.q.Next()
	if !ok {
		return false
	}
	tick, obj, _ := o.q.Read()
	ohlcv, ok := obj.(tickobjects.OHLCVObject)
	if !ok {
		panic("incorrect type")
	}
	o.O = ohlcv.Open()
	o.H = ohlcv.High()
	o.L = ohlcv.Low()
	o.C = ohlcv.Close()
	o.BuyCount = ohlcv.BuyCount()
	o.SellCount = ohlcv.SellCount()
	o.BuyVol = ohlcv.BuyVol()
	o.SellVol = ohlcv.SellVol()
	o.Price = ohlcv.Price()
	o.Time = time.UnixMilli(int64(tick))
	return true
}

type LiquidityIterator struct {
	BaseIterator
	Time time.Time
	Map  map[int64]float64
	Ts   float64
}

func (o *LiquidityIterator) Next() bool {
	ok := o.q.Next()
	if !ok {
		return false
	}
	tick, obj, _ := o.q.Read()
	ob, ok := obj.(*tickobjects.OBLiquidity)
	if !ok {
		panic("incorrect type")
	}
	o.Map = ob.Map
	o.Ts = ob.Ts
	o.Time = time.UnixMilli(int64(tick))
	return true
}

type ERC721TrackerIterator struct {
	BaseIterator
	Time    time.Time
	Tracker *tickobjects.ERC721Tracker
}

func (o *ERC721TrackerIterator) Next() bool {
	ok := o.q.Next()
	if !ok {
		return false
	}
	tick, obj, _ := o.q.Read()
	tracker, ok := obj.(*tickobjects.ERC721Tracker)
	if !ok {
		panic("incorrect type")
	}
	o.Tracker = tracker
	o.Time = time.UnixMilli(int64(tick))
	return true
}
